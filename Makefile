# see https://www.systutorials.com/how-to-get-the-full-path-and-directory-of-a-makefile-itself/
# and https://stackoverflow.com/a/23324703/13781069
ROOT_DIR:= $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))

.PHONY: clean build

MAIN_FILE = ${ROOT_DIR}/src/main.tex
OUTPUT_DIR := ${ROOT_DIR}/out

build:
		xelatex ${MAIN_FILE} \
 		-output-directory=${OUTPUT_DIR} \
 		-file-line-error \
 		-interaction=nonstopmode \
 		-synctex=1


clean:
		mv ${OUTPUT_DIR}/main.pdf ${ROOT_DIR}/memoire-ingenieur-chrys-ngoma.pdf
		rm -rvf ${OUTPUT_DIR}
